package com.example.pgirardi.loginmvp.features.login.interfaces;

/**
 * Created by p.girardi on 6/27/2016.
 */

public interface ILoginView {
    public void onLoginResult(Boolean result);
}
