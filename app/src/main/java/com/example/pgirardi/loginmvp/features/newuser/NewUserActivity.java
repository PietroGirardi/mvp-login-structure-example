package com.example.pgirardi.loginmvp.features.newuser;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.pgirardi.loginmvp.R;

public class NewUserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);
    }
}
