package com.example.pgirardi.loginmvp.features.login.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.example.pgirardi.loginmvp.R;
import com.example.pgirardi.loginmvp.features.login.interfaces.ILoginPresenter;
import com.example.pgirardi.loginmvp.features.login.interfaces.ILoginView;
import com.example.pgirardi.loginmvp.features.login.presenter.LoginPresenter;
import com.example.pgirardi.loginmvp.features.newuser.NewUserActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements ILoginView {

    @BindView(R.id.buttonLogIn)
    Button btnLogin;
    @BindView(R.id.textSignUp)
    TextView textSignUp;

    ILoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        loginPresenter = new LoginPresenter(this);
    }

    @OnClick(R.id.textSignUp)
    public void signUp(){
        Intent i = new Intent(LoginActivity.this, NewUserActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.buttonLogIn)
    public void doLogin(){
        Log.i("loginMVP", "doLogin View");
        //// TODO: 21/06/16 create style for the button, add click effect, change color
        btnLogin.setEnabled(false);
        loginPresenter.doLogin("edtName", "edtPassword");
    }


    @Override
    public void onLoginResult(Boolean result) {
        Log.i("loginMVP", "onLoginResult View");
        if(result){
            Log.i("loginMVP", "logged!!");
        }else{
            Log.i("loginMVP", "NOT AUTHORIZED!!");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        loginPresenter.onDestroy();
    }
}