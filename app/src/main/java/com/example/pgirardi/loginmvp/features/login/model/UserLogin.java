package com.example.pgirardi.loginmvp.features.login.model;

import android.util.Log;

import com.example.pgirardi.loginmvp.features.login.interfaces.ILoginModelUser;
import com.example.pgirardi.loginmvp.models.User;

/**
 * Created by p.girardi on 6/27/2016.
 */

public class UserLogin extends User implements ILoginModelUser{

    @Override
    public boolean checkUserValidity(String name, String passwd) {
        Log.i("loginMVP", "checkUserValidity Model");
        //// TODO: 6/27/2016 logic to verify permission.

        return false;
    }
}