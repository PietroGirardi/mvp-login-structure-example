package com.example.pgirardi.loginmvp.features.login.interfaces;

/**
 * Created by p.girardi on 6/27/2016.
 */

public interface ILoginPresenter {
    void doLogin(String name, String passwd);
    void setProgressBarVisibility(int visiblity);
    void onDestroy();
}
