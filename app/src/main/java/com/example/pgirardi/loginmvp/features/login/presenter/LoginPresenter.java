package com.example.pgirardi.loginmvp.features.login.presenter;

import android.util.Log;

import com.example.pgirardi.loginmvp.features.login.interfaces.ILoginModelUser;
import com.example.pgirardi.loginmvp.features.login.interfaces.ILoginPresenter;
import com.example.pgirardi.loginmvp.features.login.interfaces.ILoginView;
import com.example.pgirardi.loginmvp.features.login.model.UserLogin;

/**
 * Created by p.girardi on 6/27/2016.
 */

public class LoginPresenter implements ILoginPresenter {

    ILoginView loginView;
    ILoginModelUser user;

    public LoginPresenter(ILoginView loginView) {
        this.loginView = loginView;
    }

    @Override
    public void doLogin(String name, String passwd) {
        Log.i("loginMVP", "doLogin Presenter");
        user = new UserLogin();
        boolean auth = user.checkUserValidity(name, passwd);

        loginView.onLoginResult(auth);
    }

    @Override
    public void setProgressBarVisibility(int visiblity) {

    }

    @Override
    public void onDestroy() {
        loginView = null;
    }
}
